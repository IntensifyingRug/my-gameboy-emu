# my-gameboy-emu

A simple Gameboy emulator I'm trying to write.
Compile using `make`. Clean with `make clean`.
Im not even trying to make this cross platform right now, so don't even try on a non Linux computer.

## Requirements
* Any modern 64 bit linux distribution (may work on 32 bit but not tested)
* A semi-recent version of `gcc` and `make`

## Usage Instructions
Compile using `make` and run the file in `build` with the path of the rom as arguments and optionally the bootrom
eg. `worst_gbemu Tetris.gb bootrom.bin`
 