
#include "../include/gameboy.hpp"

// Easy ways to set flags x=arg1 y=arg2 z=carry (0 if not used)
#define setZ(x) (r.Z = x)
#define testZ(x) (r.Z = (x ? 0 : 1))
#define setN(x) (r.N = x)
#define setH(x) (r.H = x)
#define testHAdd(x, y, z) (r.H = ((((x & 0xF) + (y & 0xF) + z) > 0x0F) ? 1 : 0))
#define testHSub(x, y, z) (r.H = (((x & 0xF) < ((y & 0xF) + z)) ? 1 : 0))
#define setC(x) (r.C = x)
#define testCAdd(x, y, z) (r.C = (((x + y + z) > 0xFF) ? 1 : 0))
#define testCSub(x, y, z) (r.C = ((x < (y + z)) ? 1 : 0))

SM83::SM83(Gameboy& bus_) : bus(bus_) {
	r.af=r.bc=r.de=r.hl=r.pc=r.sp = 0; // Clear registers
}

// vvv Let's make some spaghetti! vvv
void SM83::executeOpcode() {
	opcode = fetchByte(r.pc++);

	if (opcode == 0xCB) { // Prefix CB instructions
		opcode = fetchByte(r.pc++);
		operand8 = decodeReg8(opcode);
		switch (opcode & 0xF8) {
			case 0x00: // RLC
				setN(0);
				setH(0);
				oldCarry = (*operand8 & 0x80) >> 7;
				setC(oldCarry);
				*operand8 = (*operand8 << 1) | oldCarry;
				setZ((*operand8) ? 0 : 1);
				break;
			case 0x08: // RRC reg8
				setN(0);
				setH(0);
				setC(*operand8 & 0x01);
				*operand8 = (*operand8 >> 1) | (r.C << 7);
				setZ((*operand8) ? 0 : 1);
				break;
			case 0x10: // RL reg8
				setN(0);
				setH(0);
				oldCarry = r.C;
				setC((*operand8 & 0x80) >> 7);
				*operand8 = (*operand8 << 1) | oldCarry;
				setZ((*operand8) ? 0 : 1);
				break;
			case 0x18: // RR reg8
				setN(0);
				setH(0);
				oldCarry = r.C;
				setC(*operand8 & 0x01);
				*operand8 = (*operand8 >> 1) | (oldCarry << 7);
				setZ((*operand8) ? 0 : 1);
				break;
			case 0x20: // SLA reg8
				setN(0);
				setH(0);
				setC((*operand8 & 0x80) >> 7);
				*operand8 = *operand8 << 1;
				setZ((*operand8) ? 0 : 1);
				break;
			case 0x28: // SRA reg8
				setN(0);
				setH(0);
				setC(*operand8 & 0x01);
				*operand8 = (*operand8 >> 1) | (*operand8 & 0x80);
				setZ((*operand8) ? 0 : 1);
				break;
			case 0x30: // SWAP reg8
				setN(0);
				setH(0);
				setC(0);
				*operand8 = (*operand8 << 4) | (*operand8 >> 4);
				setZ((*operand8) ? 0 : 1);
				break;
			case 0x38: // SRL reg8
				setN(0);
				setH(0);
				setC(*operand8 & 0x1);
				*operand8 = *operand8 >> 1;
				setZ((*operand8) ? 0 : 1);
				break;
		}
		if ((opcode >= 0x80) && (opcode <= 0xBF)) { // RES bit, reg8
			*operand8 &= (0xFF -(1 << ((opcode >> 3) & 0x07)));
		}
		if (opcode >= 0xC0) { // SET bit, reg8
			*operand8 |= (1 << ((opcode >> 3) & 0x07));
		}
		if ((opcode & 0x07) == 6)
			writeByte(r.hl, memOperand8);
		if ((opcode >= 0x40) && (opcode <= 0x7F)) { // BIT bit, reg8
			setZ((*operand8 & (1 << ((opcode >> 3) & 0x07))) ? false : true);
			setN(0);
			setH(1);
			return;
		}
		return;
	}

	// Take care of instructions with only one opcode or don't have a pattern
	switch (opcode) {
	case 0x00: // NOP
		return;
	case 0x02: // LD (BC), A
		writeByte(r.bc, r.a);
		return;
	case 0x07: // RLCA
		setN(0);
		setH(0);
		oldCarry = (r.a & 0x80) >> 7;
		setC(oldCarry);
		r.a = (r.a << 1) | oldCarry;
		setZ(0);
		return;
	case 0x08: // LD (imm16), SP
		bus.write16(bus.read16(r.pc), r.sp);
		r.pc += 2;
		bus.cpu.counter += 16;
		return;
	case 0x0A: // LD A, (BC)
		r.a = fetchByte(r.bc);
		return;
	case 0x0F: // RRCA
		setN(0);
		setH(0);
		setC(r.a & 0x01);
		r.a = (r.a >> 1) | (r.C << 7);
		setZ(0);
		return;
	case 0x10: // STOP
		bus.cpu.stopped = true;
		++r.pc;
		return;
	case 0x12: // LD (DE), A
		writeByte(r.de, r.a);
		return;
	case 0x17: // RLA
		setN(0);
		setH(0);
		oldCarry = r.C;
		setC((r.a & 0x80) >> 7);
		r.a = (r.a << 1) | oldCarry;
		setZ(0);
		return;
	case 0x18: // JR imm8
		r.pc += (int8_t)fetchByte(r.pc++);
		bus.cpu.counter += 4;
		return;
	case 0x1A: // LD A, (DE)
		r.a = fetchByte(r.de);
		return;
	case 0x1F: // RRA
		setN(0);
		setH(0);
		oldCarry = r.C;
		setC(r.a & 0x01);
		r.a = (r.a >> 1) | (oldCarry << 7);
		setZ(0);
		return;
	case 0x22: // LD (HL+), A
		writeByte(r.hl++, r.a);
		return;
	case 0x27: // DAA
		// I gave up trying to understand this instruction so I just copied this code from some forum
		if (!r.N) {
			if (r.C || r.a > 0x99) {
				r.a += 0x60;
				setC(1);
			}
			if (r.H || (r.a & 0x0f) > 0x09)
				r.a += 0x6;
		} else {
			if (r.C)
				r.a -= 0x60;
			if (r.H)
				r.a -= 0x6;
		}
		setZ(r.a?0:1);
		setH(0);
		return;
	case 0x2A: // LD A, (HL+)
		r.a = fetchByte(r.hl++);
		return;
	case 0x2F: // CPL
		setN(1);
		setH(1);
		r.a = ~r.a;
		return;
	case 0x32: // LD (HL-), A
		writeByte(r.hl--, r.a);
		return;
	case 0x34: // INC (HL)
		memOperand8 = fetchByte(r.hl);
		setN(0);
		testHAdd(memOperand8, 1, 0);
		testZ(++memOperand8);
		writeByte(r.hl, memOperand8);
		return;
	case 0x35: // DEC (HL)
		memOperand8 = fetchByte(r.hl);
		setN(1);
		testHSub(memOperand8, 1, 0);
		testZ(--memOperand8);
		writeByte(r.hl, memOperand8);
		return;
	case 0x37: // SCF
		setN(0);
		setH(0);
		setC(1);
		return;
	case 0x3A: // LD A, (HL-)
		r.a = fetchByte(r.hl--);
		return;
	case 0x3F: // CCF
		setN(0);
		setH(0);
		setC(~r.C);
		return;
	case 0x76: // HALT
		bus.cpu.halted = true;
		return;
	case 0xC3: // JP imm16
		r.pc = fetchWord(r.pc);
		bus.cpu.counter += 4;
		return;
	case 0xC9: // RET
		r.pc = bus.pop16();
		bus.cpu.counter += 12;
		return;
	case 0xCD: // CALL imm16
		bus.push16(r.pc+2);
		r.pc = bus.read16(r.pc);
		bus.cpu.counter += 20;
		return;
	case 0xD9: // RETI
		r.pc = bus.pop16();
		bus.cpu.interruptMasterEnable = true;
		bus.cpu.counter += 12;
		return;
	case 0xE0: // LD (FF00+imm8), A
		writeByte((0xFF00 + fetchByte(r.pc++)), r.a);
		return;
	case 0xE2: // LD (FF00+C), A
		writeByte((0xFF00 + r.c), r.a);
		return;
	case 0xE8: // ADD SP, imm8
		setZ(0);
		setN(0);
		memOperand8 = fetchByte(r.pc++);
		setC((((r.sp & 0xFF) + ((int8_t)memOperand8 & 0xFF)) > 0xFF) ? 1 : 0);
		testHAdd(r.sp, (int8_t)memOperand8, 0);
		r.sp += (int8_t)memOperand8;
		bus.cpu.counter += 8;
		return;
	case 0xE9: // JP HL
		r.pc = r.hl;
		return;
	case 0xEA: // LD (imm16), A
		writeByte(fetchWord(r.pc), r.a);
		r.pc += 2;
		return;
	case 0xF0: // LD A, (FF00+imm8)
		r.a = fetchByte(0xFF00 + fetchByte(r.pc++));
		return;
	case 0xF2: // LD A, (FF00+C)
		r.a = fetchByte(0xFF00 + r.c);
		return;
	case 0xF3: // DI
		bus.cpu.interruptMasterEnable = false;
		return;
	case 0xF8: // LD HL, SP+imm8
		setZ(0);
		setN(0);
		memOperand8 = fetchByte(r.pc++);
		setC((((r.sp & 0xFF) + ((int8_t)memOperand8 & 0xFF)) > 0xFF) ? 1 : 0);
		testHAdd(r.sp, (int8_t)memOperand8, 0);
		r.hl = r.sp + (int8_t)memOperand8;
		bus.cpu.counter += 4;
		return;
	case 0xF9: // LD SP, HL
		r.sp = r.hl;
		bus.cpu.counter += 4;
		return;
	case 0xFA: // LD A, (imm16)
		r.a = fetchByte(fetchWord(r.pc));
		r.pc += 2;
		return;
	case 0xFB: // EI
		bus.cpu.interruptMasterEnable = true;
		return;
	// Illegal opcodes
	case 0xD3:
	case 0xDB:
	case 0xDD:
	case 0xE3:
	case 0xE4:
	case 0xEB:
	case 0xEC:
	case 0xED:
	case 0xF4:
	case 0xFC:
	case 0xFD:
		printf("Opcode 0x%02X not found. Memory: 0x%04X\n", opcode, r.pc - 1);
		bus.cpu.stopped = true;
		return;
	}

	if (opcode <= 0x3F) {
		if (((opcode & 0x07) >= 1) && ((opcode & 0x07) <= 3)) {
			operand16 = decodeReg16(opcode);
			switch (opcode & 0x0F) {
			case 0x1: // LD reg16, u16
				*operand16 = fetchWord(r.pc);
				r.pc += 2;
				return;
			case 0x3: // INC reg16
				++*operand16;
				bus.cpu.counter += 4;
				return;
			case 0x9: // ADD HL, reg16
				setN(0);
				setC(((r.hl + *operand16) > 0xFFFF) ? 1 : 0);
				setH((((r.hl & 0xFFF) + (*operand16 & 0xFFF)) > 0x0FFF) ? 1 : 0);
				r.hl += *operand16;
				bus.cpu.counter += 4;
				return;
			case 0xB: // DEC reg16
				--*operand16;
				bus.cpu.counter += 4;
				return;
			}
		}
		switch (opcode & 0x07) {
		case 0: // JR cond, imm8
			memOperand8 = fetchByte(r.pc++);
			if (checkBranchCondition(opcode)) {
				r.pc += (int8_t)memOperand8;
				bus.cpu.counter += 4;
			}
			return;
		case 4: // INC reg8
			operand8 = decodeReg8(opcode >> 3);
			setN(0);
			testHAdd(*operand8, 1, 0);
			testZ(++*operand8);
			return;
		case 5: // DEC reg8
			operand8 = decodeReg8(opcode >> 3);
			setN(1);
			testHSub(*operand8, 1, 0);
			testZ(--*operand8);
			return;
		case 6: // LD reg8, imm8
			memOperand8 = fetchByte(r.pc++);
			operand8 = &memOperand8;
			if (opcode == 0x36)
				goto ops_ld1;
			goto ops_ld2;
			break;
		}
	}

	if ((opcode >= 0x40) && (opcode <= 0xBF)) { // All take the same operand
		operand8 = decodeReg8(opcode);
		if (opcode <= 0x7F) { // LD instructions
			if ((opcode >= 0x70) && (opcode <= 0x77)) { // LD (HL), reg8
				ops_ld1:
				writeByte(r.hl, *operand8);
				return;
			}
			// LD reg8, reg8
			ops_ld2:
			*decodeReg8(opcode >> 3) = *operand8;
			return;
		}
		ops_alu:
		switch (opcode & 0xF8) { // 8-bit ALU operations
		case 0x80: // ADD A, reg8
			setN(0);
			testCAdd(r.a, *operand8, 0);
			testHAdd(r.a, *operand8, 0);
			r.a += *operand8;
			testZ(r.a);
			return;
		case 0x88: // ADC A, reg8
			setN(0);
			oldCarry = r.C;
			testCAdd(r.a, *operand8, oldCarry);
			testHAdd(r.a, *operand8, oldCarry);
			r.a += (*operand8 + oldCarry);
			testZ(r.a);
			return;
		case 0x90: // SUB A, reg8
			setN(1);
			testCSub(r.a, *operand8, 0);
			testHSub(r.a, *operand8, 0);
			r.a -= *operand8;
			testZ(r.a);
			return;
		case 0x98: // SBC A, reg8
			setN(1);
			oldCarry = r.C;
			testCSub(r.a, *operand8, oldCarry);
			testHSub(r.a, *operand8, oldCarry);
			r.a -= (*operand8 + oldCarry);
			testZ(r.a);
			return;
		case 0xA0: // AND A, reg8
			setN(0);
			setH(1);
			setC(0);
			r.a &= *operand8;
			testZ(r.a);
			return;
		case 0xA8: // XOR A, reg8
			setN(0);
			setH(0);
			setC(0);
			r.a ^= *operand8;
			testZ(r.a);
			return;
		case 0xB0: // OR A, reg8
			setN(0);
			setH(0);
			setC(0);
			r.a |= *operand8;
			testZ(r.a);
			return;
		case 0xB8: // CP A, reg8
			setN(1);
			testCSub(r.a, *operand8, 0);
			testHSub(r.a, *operand8, 0);
			testZ((uint8_t)(r.a - *operand8));
			return;
		}
	}

	if (opcode >= 0xC0) {
		switch (opcode & 0x07) {
		case 0: // RET cond
			if (checkBranchCondition(opcode)) {
				r.pc = bus.pop16();
				bus.cpu.counter += 20;
			} else {
				bus.cpu.counter += 8;
			}
			return;
		case 1: // POP reg16
			((opcode == 0xF1) ? r.af : *decodeReg16(opcode)) = bus.pop16();
			r.f &= 0xF0; // Undo writes to bottom nibble of flags register
			bus.cpu.counter += 8;
			return;
		case 2: // JP cond, imm16
			memOperand16 = fetchWord(r.pc);
			r.pc += 2;
			if (checkBranchCondition(opcode)) {
				r.pc = memOperand16;
				bus.cpu.counter += 4;
			}
			return;
		case 4: // CALL cond, imm16
			memOperand16 = fetchWord(r.pc);
			r.pc += 2;
			if (checkBranchCondition(opcode)) {
				bus.push16(r.pc);
				r.pc = memOperand16;
				bus.cpu.counter += 12;
			}
			return;
		case 5: // PUSH reg16
			bus.push16((opcode == 0xF5) ? r.af : *decodeReg16(opcode));
			bus.cpu.counter += 12;
			return;
		case 6: // ALU A, imm8
			memOperand8 = fetchByte(r.pc++);
			operand8 = &memOperand8;
			opcode -= 0x40;
			goto ops_alu;
			break;
		case 7: // RST vector
			bus.push16(r.pc);
			r.pc = (opcode - 0xC7);
			bus.cpu.counter += 12;
			return;
		}
	}
}

// Returns a pointer to the register decoded from 3 bits
uint8_t *SM83::decodeReg8(uint8_t opcode_) {
	switch (opcode_ & 0x07) {
	case 0:return &r.b;
	case 1:return &r.c;
	case 2:return &r.d;
	case 3:return &r.e;
	case 4:return &r.h;
	case 5:return &r.l;
	case 6:memOperand8 = fetchByte(r.hl);return &memOperand8;
	case 7:return &r.a;
	default:return nullptr;
	}
}

uint16_t *SM83::decodeReg16(uint8_t opcode_) {
	switch ((opcode_ >> 4) & 0x03) {
	case 0:return &r.bc;
	case 1:return &r.de;
	case 2:return &r.hl;
	case 3:return &r.sp;
	default:return nullptr;
	}
}

bool SM83::checkBranchCondition(uint8_t opcode_) {
	switch ((opcode_ & 0x18) >> 3) {
	case 0:return (r.Z ? false : true); // NZ
	case 1:return (r.Z ? true : false); // Z
	case 2:return (r.C ? false : true); // NC
	case 3:return (r.C ? true : false); // C
	default:return false;
	}
}

// Reads/writes that take care of timing
uint8_t SM83::fetchByte(uint16_t address) {
	bus.cpu.counter += 4;
	return bus.read8(address);
}

uint16_t SM83::fetchWord(uint16_t address) {
	bus.cpu.counter += 16;
	return bus.read16(address);
}

void SM83::writeByte(uint16_t address, uint8_t value) {
	bus.cpu.counter += 4;
	return bus.write8(address, value);
}
