#ifndef GAMEBOY_HPP
#define GAMEBOY_HPP

#include <stdio.h>
#include <unistd.h>
#include <cstring>
#include <stdlib.h>
#include <stdint.h>
#include <iostream>
#include <vector>
#include <fstream>
#include <iterator>

#include "apu.hpp"
#include "cpu.hpp"
#include "ppu.hpp"
#include "cartridge.hpp"
#include "timer.hpp"
#include "ram.hpp"

class Gameboy {
public:
	GameboyCartridge rom;
	GameboyCPU cpu;
	GameboyPPU ppu;
	GameboyRAM ram;

	Gameboy(void (*joypadWrite_)(uint8_t), uint8_t (*joypadRead_)(), void (*serialWrite_)(uint16_t, uint8_t), uint8_t (*serialRead_)(uint16_t));
	void cycle();

	// Memory functions
	void write8(uint16_t address, uint8_t value);
	void write16(uint16_t address, uint16_t value);
	uint8_t read8(uint16_t address);
	uint16_t read16(uint16_t address);
	void push8(uint8_t value);
	void push16(uint16_t value);
	uint8_t pop8(void);
	uint16_t pop16(void);

	// Callback functions because some things rely too much on the platform
	void (*joypadWrite)(uint8_t value);
	uint8_t (*joypadRead)();
	void (*serialWrite)(uint16_t address, uint8_t value);
	uint8_t (*serialRead)(uint16_t address);

	int dmaCountdown; // 1 = Ready 2 = Waiting to finnish instruction
	int dmaCyclesLeft;

private:
};

#endif
